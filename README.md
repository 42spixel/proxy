# **Proxy**

Nginx reverse proxy for Docker.

### **Credits**

+ [jwilder/nginx-proxy](https://github.com/jwilder/nginx-proxy)

### **MIT License**

This work is licensed under the terms of **[MIT License](https://bitbucket.org/42spixel/proxy/src/master/LICENSE.md)**.
